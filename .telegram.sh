#!/bin/bash

URL="https://api.telegram.org/bot$TG_BOT_TOKEN/sendMessage"
TEXT="Deploy status: $1%0AProject: $CI_PROJECT_NAME%0AURL: $CI_PROJECT_URL/pipelines/$CI_PIPELINE_ID/%0ABranch: $CI_COMMIT_REF_SLUG"

if [ $1 = "OK" ]
then
	TEXT=$TEXT"%0A%0AClick here to /destroy"
fi

curl -s -d "chat_id=$TG_CHAT_ID&disable_web_page_preview=1&text=$TEXT" $URL > /dev/null


