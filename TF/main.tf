terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  backend "remote" {
    organization = "rel1ct0"
    workspaces {
      name = "OtusDZ"
    }
  }
}


provider "yandex" {
  cloud_id                 = var.cloud_id
  folder_id                = var.folder_id
  zone                     = var.zone
}




